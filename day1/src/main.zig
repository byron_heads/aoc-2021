const std = @import("std");

// Input is just the source data as an arry
// pub const input = [_]i32{ #, #, #, #, # };
const input = @import("input.zig").input;

pub fn main() void {
    var cnt1: i32 = 0;
    var cnt2: i32 = 0;

    var idx: usize = 1;
    while (idx < input.len) : (idx += 1) {
        // Count  Part 1
        // Skip the first value, then just compare with the prev
        if (input[idx] > input[idx - 1]) cnt1 += 1;

        // Part 2
        // dont compute if not enough data for a window
        if (idx < input.len - 2) {
            // Skip the first window, then just compute the prev window with the current and compare
            const p = input[idx - 1] + input[idx] + input[idx + 1];
            const c = input[idx] + input[idx + 1] + input[idx + 2];
            if (c > p) cnt2 += 1;
        }
    }

    std.log.debug("Count Part1: {d}", .{cnt1});
    std.log.debug("Count Part2: {d}", .{cnt2});
}
