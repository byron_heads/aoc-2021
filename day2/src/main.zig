const std = @import("std");
const mem = std.mem;
const fmt = std.fmt;

const input: []const u8 = @embedFile("input.txt");

const sample: []const u8 =
    \\ forward 5
    \\ down 5
    \\ forward 8
    \\ up 3
    \\ down 8
    \\ forward 2
;

pub fn main() anyerror!void {
    std.log.info("Part 1: {d}", .{try drive(input, true)});
    std.log.info("Part 2: {d}", .{try drive(input, false)});
}

test "part1 sample test" {
    try std.testing.expectEqual(try drive(sample, true), 150);
    try std.testing.expectEqual(try drive(input, true), 1250395);
}

test "part2 sample test" {
    try std.testing.expectEqual(try drive(sample, false), 900);
    try std.testing.expectEqual(try drive(input, false), 1451210346);
}

const FORWARD = "forward";
const DOWN = "down";
const UP = "up";

fn drive(inst: []const u8, part1: bool) !i32 {
    var h: i32 = 0;
    var d: i32 = 0;
    var aim: i32 = 0;

    var it = mem.tokenize(u8, inst, "\n");
    while (it.next()) |pline| {
        const line = mem.trim(u8, pline, " \r\t\n"); // clean it up
        if (mem.startsWith(u8, line, FORWARD)) {
            const val = try fmt.parseInt(u8, line[FORWARD.len + 1 ..], 10);
            h += val;
            if (!part1) d += aim * val;
        } else if (mem.startsWith(u8, line, DOWN)) {
            const val = try fmt.parseInt(u8, line[DOWN.len + 1 ..], 10);
            if (part1) {
                d += val;
            } else aim += val;
        } else if (mem.startsWith(u8, line, UP)) {
            const val = try fmt.parseInt(u8, line[UP.len + 1 ..], 10);
            if (part1) {
                d -= val;
            } else aim -= val;
        }
    }

    return h * d;
}
